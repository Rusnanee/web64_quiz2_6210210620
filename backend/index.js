const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'bizmall',
    password : 'bizmall',
    database : 'BizMall'
})

connection.connect();

const express = require('express');
const app = express()
const port = 7000

app.post("/add_user", (req, res) => {

    let user_name = req.query.user_name
    let user_address = req.query.user_address
    let user_tel = req.query.user_tel

    let query = ` INSERT INTO User 
                (User_name, User_address, User_tel)
                VALUES ('${user_name}',
                        '${user_address}',
                        '${user_tel}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into database BizMall"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Adding user succesful"
            })
        }
    });
})

app.post("/list_user", (req, res) => {
    query = "SELECT * from User";

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from user name in database BizMall"     
            })
        }
        else{
            res.json(rows)
        }
    });
})

app.post("/update_user", (req, res) => {

    let user_name = req.query.user_name
    let user_address = req.query.user_address
    let user_tel = req.query.user_tel
    let user_id = req.query.user_id

    let query = ` UPDATE User SET
                                user_name='${user_name}', 
                                user_address='${user_address}', 
                                user_tel='${user_tel}'
                                WHERE user_id=${user_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Updating user succesful"
            })
        }
    });
})

app.post("/delete_user", (req, res) => {

    let user_id = req.query.user_id

    let query = ` DELETE FROM User WHERE user_id=${user_id}`
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Deleting event succesful"
            })
        }
    });
})

app.post("/add_register", (req, res) => {

    let reserve_id = req.query.reserve_id
    let date = req.query.date
    let user_id = req.query.user_id
    let id = req.query.id

    let query = `INSERT INTO Reserve (reserve_id, date, user_id, id) VALUES ('${reserve_id}', '${date}', '${user_id}', '${id}')`
    console.log(query)
    
    connection.query( query, (err, rows) => {
        if(err) {
            res.json({ 
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                "status" : "200",
                "message" : "Adding Reserve succesful"
            })
        }    
    });
})

app.post("/list_register", (req, res) => {
    query = "SELECT * from Reserve";

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from Reserve name in database BizMall"     
            })
        }
        else{
            res.json(rows)
        }
    });
})

app.post("/update_register", (req, res) => {

    let date = req.query.date
    let reserve_id = req.query.reserve_id

    let query = ` UPDATE Reserve SET date=${date} WHERE reserve_id=${reserve_id};`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Updating Reserve succesful"
            })
        }
    });
})

app.post("/delete_register", (req, res) => {

    let reserve_id = req.query.reserve_id

    let query = ` DELETE FROM Reserve WHERE reserve_id=${reserve_id}`
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Deleting Reserve succesful"
            })
        }
    });
})

app.listen(port, () => {
    console.log(` Now starting Reserve BizMall System Backend ${port} `)
})

